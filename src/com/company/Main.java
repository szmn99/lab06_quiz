package com.company;

public class Main {

    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();

        int min_val = quiz.MIN_VALUE;
        int max_val = quiz.MAX_VALUE;

        int digit = (min_val + max_val)/2;  // wiem że kod się powtarza, ale przy tak krótkim zdaniu nie ma chyba sensu robić osobnej metody :)

        for(int counter = 1; ;counter++) {
            try {
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;
            } catch (Quiz.ParamTooLarge l) {
                System.out.println("Argument za duzy!!!");
                max_val = digit;
                digit = (min_val + max_val)/2;
            } catch (Quiz.ParamTooSmall s) {
                System.out.println("Argument za maly!!!");
                min_val = digit;
                digit = (min_val + max_val)/2;
            }
        }
    }

}
